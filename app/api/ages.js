module.exports = () => {
  const ContactAgesDao = require('../Dao/ages-dao');

  const contacts = {
    get(req, res, next) {

      new ContactAgesDao(req.connection)
        .retornarIdades()
        .then(contacts => res.json(contacts))
        .catch(next)

    },
  };
  return contacts;
};