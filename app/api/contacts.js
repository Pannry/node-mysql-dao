module.exports = () => {
  const ProductDao = require('../Dao/contacts-dao');

  const contacts = {
    get(req, res, next) {

      new ProductDao(req.connection)
        .retornarContatos()
        .then(contacts => res.json(contacts))
        .catch(next)

    },
  };
  return contacts;
};