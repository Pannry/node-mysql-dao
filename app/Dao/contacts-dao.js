module.exports = class ContactsDao {
  constructor(connection) {
    this._connection = connection;
  }

  retornarContatos() {
    return new Promise((resolve, reject) =>
      this._connection.query('SELECT * FROM contato', (err, contacts) => {
        if (err) return reject(err);
        resolve(contacts);
      })
    );
  }
}
