module.exports = class ContactsAgesDao {
  constructor(connection) {
    this._connection = connection;
  }

  retornarIdades() {
    return new Promise((resolve, reject) =>
      this._connection.query('SELECT idade FROM contato', (err, contacts) => {
        if (err) return reject(err);
        resolve(contacts);
      })
    );
  }
}
