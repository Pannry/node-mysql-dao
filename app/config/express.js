var express = require('express');
const consign = require('consign');

const pool = require('../infra/pool-factory');
const connectionMiddleware = require('../infra/connection-middleware');

module.exports = () => {
  // CONFIGURANDO SERVIDOR
  const app = express();

  app.set('port', 3000);

  // app.use(connectionMiddleware(pool));

  consign({ cwd: 'app' })
    .include('api')
    .then('infra')
    .into(app)
  // FIM DA CONFIGURAÇÃO DO SERVIDOR

  app.route('/')
    .get(connectionMiddleware(pool), app.api.contacts.get);

  app.route('/ages')
    .get(connectionMiddleware(pool), app.api.ages.get);

  

  return app;
};
